-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2022 at 04:13 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taman_belakang`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `foto_admin` varchar(100) NOT NULL,
  `alamat_admin` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_lengkap`, `foto_admin`, `alamat_admin`, `telepon`) VALUES
(1, 'admin', '801dc3c9e3bcd2a3cf428f3b79b312b6', 'Bintang Akbar', 'taman_belakang.jpeg', 'Bintara Jaya', '082260820643');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Aneka Nasi'),
(2, 'Nasi Goreng'),
(3, 'Nasi Ayam'),
(4, 'Nastel'),
(5, 'Magelangan'),
(6, 'Aneka Mie'),
(7, 'Snack'),
(8, 'Keju Aroma'),
(9, 'Aneka Bakso'),
(10, 'Aneka Sate'),
(11, 'Minuman Panas'),
(12, 'Minuman Dingin');

-- --------------------------------------------------------

--
-- Table structure for table `ongkir`
--

CREATE TABLE `ongkir` (
  `id_ongkir` int(5) NOT NULL,
  `nama_kota` varchar(100) NOT NULL,
  `tarif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ongkir`
--

INSERT INTO `ongkir` (`id_ongkir`, `nama_kota`, `tarif`) VALUES
(1, 'jakarta', 9000),
(2, 'bogor', 18000);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `bukti` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_pembelian`, `nama`, `bank`, `jumlah`, `tanggal`, `bukti`) VALUES
(2, 45, 'malik', 'BCA', 21500, '2021-06-03', '20210603192721WhatsApp Image 2021-06-02 at 23.18.29.jpeg'),
(3, 45, 'malik', 'BCA', 21500, '2021-06-03', '20210603192750WhatsApp Image 2021-06-02 at 23.18.29.jpeg'),
(4, 46, 'malik', 'BCA', 124000, '2021-06-03', '20210603201346EcSYw0MU8AAYT5i.jpg'),
(5, 48, 'malik', 'BCA', 349000, '2021-06-03', '20210603203051maxresdefault.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `total_pembelian` int(11) NOT NULL,
  `nohandphone` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `status_pembelian` varchar(100) NOT NULL DEFAULT 'pending',
  `no_meja` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_penjualan`, `nama`, `tanggal_pembelian`, `total_pembelian`, `nohandphone`, `alamat`, `status_pembelian`, `no_meja`) VALUES
(85, 'Sena Bayu', '2022-06-15', 10000, '082260820643', '', 'selesai', ''),
(86, 'aaaaaa', '2022-06-15', 14000, '082260820643', '', 'selesai', ''),
(87, 'Andika', '2022-06-15', 100000, '082260820643', '', 'selesai', ''),
(88, 'Bintang', '2022-06-15', 52000, '082260820643', '', 'pending', ''),
(89, 'nxika', '2022-06-15', 37000, '082228383', '', 'pending', ''),
(90, 'Anes', '2022-06-15', 30000, '087655362558', '', 'pending', ''),
(91, 'zazazaza', '2022-06-26', 14000, '082260820643', '', 'pending', ''),
(92, 'Sena Bayu', '2022-06-26', 10000, '082260820621', '', 'pending', '001'),
(93, 'Bintang', '2022-07-16', 36000, '082260820643', '', 'pending', '1'),
(94, 'Bintang', '2022-07-16', 10000, '082260820643', '', 'pending', '6'),
(95, 'aaaa', '2022-07-16', 23000, '082260820621', '', 'pending', '1'),
(96, 'Bintang', '2022-07-16', 23000, '082260820621', '', 'pending', '1'),
(97, 'Bintang', '2022-07-16', 10000, '082260820621', '', 'pending', '1'),
(98, 'Bintang', '2022-07-16', 13000, '082260820621', '', 'pending', '6'),
(99, 'Bintang', '2022-08-05', 14000, '082260820643', '', 'pending', '1');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_produk`
--

CREATE TABLE `penjualan_produk` (
  `id_penjualan_produk` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `subberat` int(11) NOT NULL,
  `subharga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan_produk`
--

INSERT INTO `penjualan_produk` (`id_penjualan_produk`, `id_penjualan`, `id_produk`, `jumlah`, `nama`, `harga`, `berat`, `subberat`, `subharga`) VALUES
(159, 85, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(160, 86, 41, 1, 'Nasi Putih', 4000, 0, 0, 4000),
(161, 86, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(162, 87, 43, 3, 'Nasi Goreng Polos', 10000, 0, 0, 30000),
(163, 87, 45, 5, 'Nasi Goreng Telur sosis', 14000, 0, 0, 70000),
(164, 88, 44, 2, 'Nasi Goreng Telur', 13000, 0, 0, 26000),
(165, 88, 41, 3, 'Nasi Putih', 4000, 0, 0, 12000),
(166, 88, 54, 1, 'Magelangan Bakso', 14000, 0, 0, 14000),
(167, 89, 60, 1, 'Indomie Double Polos', 10000, 0, 0, 10000),
(168, 89, 61, 1, 'Indomie Double Telur', 13000, 0, 0, 13000),
(169, 89, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(170, 89, 41, 1, 'Nasi Putih', 4000, 0, 0, 4000),
(171, 90, 46, 2, 'Nasi Goreng Special', 15000, 0, 0, 30000),
(172, 91, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(173, 91, 41, 1, 'Nasi Putih', 4000, 0, 0, 4000),
(174, 0, 44, 1, 'Nasi Goreng Telur', 13000, 0, 0, 13000),
(175, 0, 41, 1, 'Nasi Putih', 4000, 0, 0, 4000),
(176, 92, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(177, 93, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(178, 93, 44, 2, 'Nasi Goreng Telur', 13000, 0, 0, 26000),
(179, 94, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(180, 95, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(181, 95, 44, 1, 'Nasi Goreng Telur', 13000, 0, 0, 13000),
(182, 96, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(183, 96, 44, 1, 'Nasi Goreng Telur', 13000, 0, 0, 13000),
(184, 97, 43, 1, 'Nasi Goreng Polos', 10000, 0, 0, 10000),
(185, 98, 44, 1, 'Nasi Goreng Telur', 13000, 0, 0, 13000),
(186, 99, 53, 1, 'Magelangan Sosis', 14000, 0, 0, 14000);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `foto_produk` varchar(100) NOT NULL,
  `deskripsi_produk` text NOT NULL,
  `stok_produk` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_kategori`, `nama_produk`, `harga`, `foto_produk`, `deskripsi_produk`, `stok_produk`) VALUES
(41, 1, 'Nasi Putih', 4000, 'nasi_putih.jpeg', 'Nasi Putih                                                            ', 96),
(43, 2, 'Nasi Goreng Polos', 10000, 'nasi_goreng.webp', 'Nasi Goreng Polos                        ', 95),
(44, 2, 'Nasi Goreng Telur', 13000, 'nasi_goreng.jpg', 'Nasi Goreng Telur                        ', 1),
(45, 2, 'Nasi Goreng Telur sosis', 14000, 'nasi_goreng.jpg', 'Nasi Goreng Telur sosis', 0),
(46, 2, 'Nasi Goreng Special', 15000, 'nasi_goreng.jpg', 'Nasi Goreng Telur Sosis Bakso                                    ', -1),
(47, 3, 'Nasi Ayam Penyet', 18000, '', 'Nasi Ayam Penyet                                                                                    ', -1),
(48, 3, 'Nasi Ayam Bakar', 18000, '', 'Nasi Ayam Bakar', 0),
(49, 4, 'Nastel Original', 8000, '', 'Nasi Telur Dadar Original', -1),
(50, 4, 'Nastel Sosis', 10000, '', 'Nasi Telur Dadar Sosis', 0),
(51, 4, 'Nastel Sosis Bakso', 12000, '', 'Nasi Telur Dadar Sosis Bakso', 0),
(52, 5, 'Magelangan Original', 13000, '', 'Magelangan Original', 0),
(53, 5, 'Magelangan Sosis', 14000, '', 'Magelangan Sosis', 0),
(54, 5, 'Magelangan Bakso', 14000, '', 'Magelangan Bakso', 0),
(55, 5, 'Magelangan Ayam', 15000, '', 'Magelangan Ayam', 0),
(56, 5, 'Magelangan Sosis Bakso Ayam', 18000, '', 'Magelangan Sosis Bakso Ayam', 0),
(57, 6, 'Indomie', 7000, '', 'Indomie', 0),
(58, 6, 'Indomie telur', 10000, '', 'Indomie telur', 0),
(59, 6, 'Mie Tek-Tek (rebus/goreng)', 12000, '', 'Mie Tek-Tek (rebus/goreng)', 0),
(60, 6, 'Indomie Double Polos', 10000, '', 'Indomie Double Polos', 0),
(61, 6, 'Indomie Double Telur', 13000, '', 'Indomie Double Telur', 0),
(62, 7, 'Roti Bakar', 8000, '', 'Roti Bakar', 0),
(63, 7, 'Stik Kentang', 10000, '', 'Stik Kentang', 0),
(64, 7, 'Gorengan Bakwan (isi 3)', 4000, '', 'Gorengan Bakwan (isi 3)', 0),
(65, 7, 'Gorengan Tahu isi (isi 3)', 4000, '', 'Gorengan Tahu isi (isi 3)', 0),
(66, 8, 'Keju Aroma Original', 10000, '', 'Keju Aroma Original', 0),
(67, 8, 'Keju Aroma Toping Cokelat', 11000, '', 'Keju Aroma Toping Cokelat', 0),
(68, 8, 'Keju Aroma Toping Cokelat Keju', 13000, '', 'Keju Aroma Toping Cokelat Keju', 0),
(69, 8, 'Keju Aroma Toping Milo', 15000, '', 'Keju Aroma Toping Milo', 0),
(70, 9, 'Bakso Biasa', 10000, '', 'Bakso Biasa', 0),
(71, 9, 'Bakso Urat', 13000, '', 'Bakso Urat', 0),
(72, 9, 'Bakso Special', 25000, '', 'Bakso Special', 0),
(73, 7, 'Cemilan Ciki', 2500, '', 'Cemilan Ciki', 0),
(74, 10, 'Sosis kecil', 2000, '', 'Sosis kecil', 0),
(75, 10, 'Otak-otak', 2000, '', 'Otak-otak', 0),
(76, 7, 'Bakso Sapi', 3000, '', 'Bakso Sapi', 0),
(77, 10, 'Fish Koin', 3000, '', 'Fish Koin', 0),
(78, 10, 'Fish Roll', 3000, '', 'Fish Roll', 0),
(79, 10, 'Dumpling Cheese', 5000, '', 'Dumpling Cheese', 0),
(80, 10, 'Sosis Besar', 8000, '', 'Sosis Besar', 0),
(81, 11, 'Kopi Kapal Api', 4000, '', 'Kopi Kapal Api', 0),
(82, 11, 'CoffeMix', 4000, '', 'CoffeMix', 0),
(83, 11, 'ABC Susu', 4000, '', 'ABC Susu', 0),
(84, 11, 'White Coffe', 4000, '', 'White Coffe', 0),
(85, 11, 'Good Day', 4000, '', 'Good Day', 0),
(86, 11, 'Milo', 4000, '', 'Milo', 0),
(87, 11, 'Chocolatos', 4000, '', 'Chocolatos', 0),
(88, 11, 'Nutrisari', 4000, '', 'Nutrisari', 0),
(89, 11, 'Wedang Jahe', 4000, '', 'Wedang Jahe', 0),
(90, 11, 'Teh Tarik', 4000, '', 'Teh Tarik', 0),
(91, 11, 'Teh Manis', 4000, '', 'Teh Manis', 0),
(92, 11, 'Teh Tawar', 2000, '', 'Teh Tawar', 0),
(93, 12, 'White Coffe Dingin', 5000, '', 'White Coffe Dingin', 0),
(94, 12, 'Milo Dingin', 5000, '', 'Milo Dingin', 0),
(95, 12, 'Chocolatos Dingin', 5000, '', 'Chocolatos Dingin', 0),
(96, 12, 'Nutrisari Dingin', 5000, '', 'Nutrisari Dingin', 0),
(97, 12, 'Tea Jus', 3000, '', 'Tea Jus Dingin', 0),
(98, 12, 'S-tee', 4000, '', 'S-tee Dingin', 0),
(99, 12, 'Fruitea', 5000, '', 'Fruitea', 0),
(100, 12, 'Es Teh Manis', 5000, '', 'Es Teh Manis', 0),
(101, 12, 'Es Teh Tawar ', 3000, '', 'Es Teh Tawar ', 0),
(102, 12, 'Air Mineral', 3000, '', 'Air Mineral', 0),
(103, 12, 'Susu Dancow', 6000, '', 'Susu Dancow Dingin', 0),
(104, 12, 'Dalgona Coffe', 10000, '', 'Dalgona Coffe', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `ongkir`
--
ALTER TABLE `ongkir`
  ADD PRIMARY KEY (`id_ongkir`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  ADD PRIMARY KEY (`id_penjualan_produk`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ongkir`
--
ALTER TABLE `ongkir`
  MODIFY `id_ongkir` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  MODIFY `id_penjualan_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
