<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert"></script>
  </head>
  <body>
    <?php
    session_start();

    $id_produk = $_GET['id'];
    $id_kategori = $_GET['id'];

    if (isset($_SESSION['keranjang'][$id_produk])) {
        $_SESSION['keranjang'][$id_produk] += 1;
    } else {
        $_SESSION['keranjang'][$id_produk] = 1;
    }

    echo "<script>alert('ditambahkan ke keranjang');</script>";
    // echo "<script>
    //           swal({
    //             title: 'Information Saved',
    //             text: 'data successfully updated!',
    //             type: 'success',
    //             html: true
    //           },
    //           function(){
    //             window.location.replace('menu.php');
    //           });
    //           </script>";
    echo "<script>location='menu.php';</script>";
?>
  </body>
</html>
