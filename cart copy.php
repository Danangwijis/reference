<?php
session_start();
include 'connection.php';
if (empty($_SESSION["keranjang"]) or !isset($_SESSION["keranjang"])) {
  echo "<script>alert('Keranjang Kosong,Silahkan Isi Keranjang');</script>";
  echo "<script>location='menu.php';</script>";
}
?>

<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

  <title> BOSQUE </title>

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <!-- nice select  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
  <!-- font awesome style -->
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />

</head>

<body class="sub_page">

  <div class="hero_area">
    <div class="bg-box">
      <img src="images/hero-bg.jpg" alt="">
    </div>
    <!-- header section strats -->
    <header class="header_section">
      <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="index.php">
            <span>
              BOSQUE
            </span>
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=""> </span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  mx-auto ">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Home </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="menu.php">Menu <span class="sr-only">(current)</span> </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
              </li>

            </ul>
            <div class="user_option">
              <a href="#" class="user_link">
              </a>
              <a href="#" class="user_link">
              </a>
              <a href="#" class="user_link">
              </a>
              <a href="#" class="user_link">
              </a>
              <a class="cart_link" href="cart.php">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                  <g>
                    <g>
                      <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                   c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                    </g>
                  </g>
                  <g>
                    <g>
                      <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                   C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                   c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                   C457.728,97.71,450.56,86.958,439.296,84.91z" />
                    </g>
                  </g>
                  <g>
                    <g>
                      <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                   c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                    </g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                </svg>
              </a>
              <a href="menu.php" class="order_online">
                Order
              </a>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <!-- end header section -->
  </div>

  <!-- food section -->

  <section class="ftco-section ftco-cart">
    <div class="container">
      <div class="row justify-content-center">
        <div class="row mx-auto justify-content-center text-center">
          <div class="col-12 mt-3 ">
            <nav aria-label="breadcrumb" class="second ">
              <ol class="breadcrumb indigo lighten-6 first ">
                <li class="breadcrumb-item font-weight-bold "><a class="black-text text-uppercase " href="product.php"><span class="mr-md-3 mr-1">Produk</span></a><i class="fa fa-angle-double-right " aria-hidden="true"></i></li>
                <li class="breadcrumb-item font-weight-bold"><a class="black-text text-uppercase" href="cart.php"><span class="mr-md-3 mr-1">Keranjang</span></a><i class="fa fa-angle-double-right text-uppercase " aria-hidden="true"></i></li>
                <li class="breadcrumb-item font-weight-bold"><a class="black-text text-uppercase active-2" href="checkout.php"><span class="mr-md-3 mr-1">CHECKOUT</span></a></li>
              </ol>
            </nav>
          </div>
        </div>
        <div class="">
          <table class="table">
            <thead class="thead-primary">
              <tr class="text-center">
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Nama Produk</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Total</th>

              </tr>
            </thead>
            <tbody>
              <?php $totalbelanja = 0; ?>
              <?php foreach ($_SESSION["keranjang"] as $id_produk => $jumlah) :
                $sql = "SELECT * FROM produk WHERE id_produk='$id_produk'";
                $qry = mysqli_query($koneksi, $sql);
                while ($row = mysqli_fetch_array($qry)) {
                  $id_produk = $row['id_produk'];
                  $id_kategori = $row['id_kategori'];
                  $nama_produk = $row['nama_produk'];
                  $harga = $row['harga'];
                  $foto_produk = $row['foto_produk'];
                  $deskripsi_produk = $row['deskripsi_produk'];
                  $stok_produk = $row['stok_produk'];

                  $subharga = $row["harga"] * $jumlah;
              ?>
                  <tr class="text-center">
                    <td class="product-remove"><a href="hapuskeranjang.php?id=<?php echo $id_produk ?>"><span class="ion-ios-close"></span></a></td>

                    <td class="image-prod">
                      <div class="img" style="background-image:url('images/BOSQUE/<?php echo $row['foto_produk']; ?>');"></div>
                    </td>

                    <td class="product-name">

                      <h3>
                        <?php echo $row['nama_produk']; ?>
                      </h3>
                      <p>
                        <?php echo $row['deskripsi_produk']; ?>
                      </p>
                    </td>
                    <td class="price">
                      Rp. <?php echo  $row['harga'] ?>
                    </td>
                    <td class="quantity">
                      <div class="input-group mb-3">
                        <input type="text" name="quantity" class="quantity form-control input-number" value="<?php echo $jumlah ?>" min="1" max="100">
                      </div>
                    </td>

                    <td class="total"><?php echo number_format($subharga); ?></td>

                  </tr>
                  <?php $totalbelanja += $subharga ?>
                <?php } ?>
              <?php endforeach ?>
            </tbody>
          </table>
          <div class="row justify-content-end">
            <p><a href="product.php" class="btn btn-primary py-3 px-4">Tambah barang</a></p>
          </div>
        </div>


        <!-- <div class="btn-box">
        <a href="">
          View More
        </a>
      </div> -->

      </div>
      <div class="row justify-content-end">
        <div class="col-lg-4 mt-5 cart-wrap ftco-animate">

          <div class="cart-total mb-3">
            <h3>Total</h3>
            <p class="d-flex">
              <span>Total Sementara</span>
              <span><?php echo number_format($totalbelanja); ?></span>
            </p>
            <p class="d-flex">
              <span>Biaya Kirim</span>
              <span>Gratis</span>
            </p>
            <hr>
            <p class="d-flex total-price">
              <span>Total</span>
              <span><?php echo number_format($totalbelanja); ?></span>
            </p>
          </div>

        </div>
      </div>
      <div class="row justify-content-end">
        <p><a href="checkout.php" class="btn btn-primary py-3 px-4">Checkout</a></p>
      </div>
    </div>
  </section>

  <!-- end food section -->

  <!-- footer section -->
  <footer class="footer_section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 footer-col">
          <div class="footer_contact">
            <h4>
              Contact Us
            </h4>
            <div class="contact_link_box">
              <a href="about.php">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span>
                  Cililitan
                </span>
              </a>
              <a href="https://wa.me/6281779812662">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span>
                  +6281779812662
                </span>
              </a>
              <a href="mailto:bosque@gmail.com">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span>
                  bosque@gmail.com
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-col">
          <div class="footer_detail">
            <a href="" class="footer-logo">
              BOSQUE
            </a>

            <div class="footer_social">
              <a href="https://www.instagram.com/tamanbelakang.gas/" target="_blank">
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-col">
          <h4>
            Jam Buka
          </h4>
          <p>
            Setiap Hari
          </p>
          <p>
            11.00 Pagi -10.00 malam
          </p>
        </div>
      </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> All Rights Reserved By BOSQUE<br><br>
        </p>
      </div>
    </div>
  </footer>
  <!-- footer section -->

  <!-- jQery -->
  <script src="js/jquery-3.4.1.min.js"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script src="js/bootstrap.js"></script>
  <!-- owl slider -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- isotope js -->
  <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
  <!-- nice select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
  <!-- custom js -->
  <script src="js/custom.js"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
  </script>
  <!-- End Google Map -->

</body>

</html>