<?php
session_start();
include 'connection.php';
if (empty($_SESSION["keranjang"]) or !isset($_SESSION["keranjang"])) {
  echo "<script>alert('Keranjang Kosong,Silahkan Isi Keranjang');</script>";
  echo "<script>location='menu.php';</script>";
}
?>

<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

  <title> Taman Belakang </title>

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <!-- nice select  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
  <!-- font awesome style -->
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="css/animate.css">

  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">

  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/ionicons.min.css">

  <link rel="stylesheet" href="css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="css/jquery.timepicker.css">


  <link rel="stylesheet" href="css/flaticon.css">
  <link rel="stylesheet" href="css/icomoon.css">
</head>

<body class="sub_page">

  <div class="hero_area">
    <div class="bg-box">
      <img src="images/hero-bg.jpg" alt="">
    </div>
    <!-- header section strats -->
    <header class="header_section">
      <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="index.php">
            <span>
              Taman Belakang
            </span>
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=""> </span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  mx-auto ">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Home </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="menu.php">Menu <span class="sr-only">(current)</span> </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
              </li>

            </ul>
            <div class="user_option">
              <a href="#" class="user_link">
              </a>
              <a href="#" class="user_link">
              </a>
              <a href="#" class="user_link">
              </a>
              <a href="#" class="user_link">
              </a>
              <a class="cart_link" href="cart.php">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                  <g>
                    <g>
                      <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                   c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                    </g>
                  </g>
                  <g>
                    <g>
                      <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                   C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                   c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                   C457.728,97.71,450.56,86.958,439.296,84.91z" />
                    </g>
                  </g>
                  <g>
                    <g>
                      <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                   c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                    </g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                </svg>
              </a>
              <a href="menu.php" class="order_online">
                Order
              </a>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <!-- end header section -->
  </div>

  <!-- food section -->
  <br>
  <br>

  <form method="post">
    <div class=" container-fluid my-5 ">
      <div class="row justify-content-center ">
        <div class="col-xl-10">
          <div class="card shadow-lg ">

            <div class="row mx-auto justify-content-center text-center">
              <div class="col-12 mt-3 ">
                <nav aria-label="breadcrumb" class="second ">
                  <ol class="breadcrumb indigo lighten-6 first ">
                    <li class="breadcrumb-item font-weight-bold "><a class="black-text text-uppercase " href="menu.php"><span class="mr-md-3 mr-1">Produk</span></a><i class="fa fa-angle-double-right " aria-hidden="true"></i></li>
                    <li class="breadcrumb-item font-weight-bold"><a class="black-text text-uppercase" href="cart.php"><span class="mr-md-3 mr-1">Keranjang</span></a><i class="fa fa-angle-double-right text-uppercase " aria-hidden="true"></i></li>
                    <li class="breadcrumb-item font-weight-bold"><a class="black-text text-uppercase active-2" href="checkout.php"><span class="mr-md-3 mr-1">CHECKOUT</span></a></li>
                  </ol>
                </nav>
              </div>
            </div>
            <div class="row justify-content-around">
              <div class="col-md-5">
                <div class="card border-0">

                  <div class="card-body">

                    <div class="row mt-4">
                      <div class="col">
                        <p class="text-muted mb-2">Detail Pelanggan</p>
                        <hr class="mt-0">
                      </div>
                    </div>
                    <div class="form-group"> <label for="NAME" class="small text-muted mb-1">NAMA </label>
                      <input type="text" class="form-control form-control-sm" name="nama" id="NAME" aria-describedby="helpId" placeholder="Nama" required>
                    </div>
                    <div class="form-group"> <label for="NAME" class="small text-muted mb-1">No Handphone</label>
                      <input type="text" class="form-control form-control-sm" name="nohandphone" id="NAME" aria-describedby="helpId" placeholder="handphone" required>
                    </div>
                      <label for="NAME" class="small text-muted mb-1">No Meja</label>
                    <div class="form-group">
                          <select name='no_meja' class='form-control form-control-sm' required>
                                  <?php
                                  for ($i=1; $i <= 40; $i++) { ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                              <?php    }
                                   ?>
                          </select>
                    </div>
                    <br><br>

                    <div class="form-group" style="">
                          <select name='methodpembayaran' class='' required>
                                  <option value="0">Pembayaran</option>
                                  <option value="cash">cash</option>
                                  <option value="cashless">cashless</option>
                          </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="card border-0 ">
                  <div class="card-header card-2">
                    <p class="card-text text-muted mt-md-4 mb-2 space">Pembelianmu </p>
                    <hr class="my-2">
                  </div>
                  <br>
                  <?php $totalbelanja = 0; ?>
                  <?php foreach ($_SESSION["keranjang"] as $id_produk => $jumlah) :
                    $sql = "SELECT * FROM produk WHERE id_produk='$id_produk'";
                    $qry = mysqli_query($koneksi, $sql);
                    while ($row = mysqli_fetch_array($qry)) {
                      $id_produk = $row['id_produk'];
                      $id_kategori = $row['id_kategori'];
                      $nama_produk = $row['nama_produk'];
                      $harga = $row['harga'];
                      $foto_produk = $row['foto_produk'];
                      $deskripsi_produk = $row['deskripsi_produk'];
                      // $stok_produk = $row['stok_produk'];

                      $subharga = $row["harga"] * $jumlah;
                  ?>

                      <div class="card-body pt-0">
                        <div class="row justify-content-between">
                          <div class="col-auto col-md-7">
                            <br>
                            <div class="media flex-column flex-sm-row">
                              <?php
                              if($row['foto_produk'])
                              {?>
                              <img class="img-fluid" src="images/BOSQUE/<?php echo $row['foto_produk']; ?>" width="62" height="62">
                            <?php }else{?>
                                <img class="img-fluid" src="images/BOSQUE/noimg.jpg" width="62" height="62">
                              <?php } ?>
                              <div class="media-body my-auto">
                                <div class="row ">
                                  <div class="col-auto">
                                    <p class="mb-0"><b><?php echo "&nbsp;&nbsp;".$row['nama_produk']; ?></b></p><small class="text-muted"></small>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class=" pl-0 flex-sm-col col-auto my-auto">
                            <p class="boxed-1"><?php echo $jumlah ?></p>
                          </div>
                          <div class=" pl-0 flex-sm-col col-auto my-auto ">
                            <p><b><?php echo number_format($subharga); ?></b></p>
                          </div>
                        </div>
                      </div>
                      <hr class="my-2">

                      <?php $totalbelanja += $subharga ?>
                    <?php } ?>
                  <?php endforeach ?>
                  <div class="row ">
                    <div class="col">
                      <div class="row justify-content-around">
                        <div class="col-4">
                          <p class="mb-1"><b>Subtotal</b></p>
                        </div>
                        <div class="flex-sm-col col-auto">
                          <p class="mb-1"><b><?php echo number_format($totalbelanja); ?></b></p>
                        </div>
                      </div>
                      <div class="row justify-content-around">
                      </div>
                      <div class="row justify-content-around">
                        <div class="col-4">
                          <p><b>Total</b></p>
                        </div>
                        <div class="flex-sm-col col-auto">
                          <p class="mb-1"><b><?php echo number_format($totalbelanja); ?></b></p>
                        </div>
                      </div>
                      <hr class="my-0">
                    </div>
                  </div>
                  <div class="row mb-5 mt-4 ">
                    <div class="col-md-7 col-lg-4 mx-auto"><button name="pesan" class="btn btn-block btn-primary btn-lg">Pesan</button></div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </form>
  <?php
  if (isset($_POST["pesan"])) {
    $method_pembayaran = $_POST['methodpembayaran'];
    $nama_pelanggan = $_POST['nama'];
    $tanggal_pembelian = date("Y-m-d");
    $no_handphone = $_POST['nohandphone'];
    $total_pembelian = $totalbelanja;
    $no_meja = $_POST['no_meja'];
    // simpen data ke tabel pembelian //
    $koneksi->query(
      "INSERT INTO penjualan (
                    nama,tanggal_pembelian,total_pembelian,nohandphone,no_meja)
                    VALUES ('$nama_pelanggan','$tanggal_pembelian','$total_pembelian','$no_handphone','$no_meja')"
    );




    // dapetin id pembelian yang dibeli
    $id_penjualan_barusan = $koneksi->insert_id;

    foreach ($_SESSION["keranjang"] as $id_produk => $jumlah) {

      $ambil = $koneksi->query("SELECT * FROM produk WHERE id_produk='$id_produk'");
      $perproduk = $ambil->fetch_assoc();

      $nama_produk = $perproduk['nama_produk'];
      $harga = $perproduk['harga'];
      $berat = $perproduk['berat_produk'];

      $subharga = $perproduk['harga'] * $jumlah;
      $koneksi->query("INSERT INTO penjualan_produk (id_penjualan,id_produk,jumlah,nama,harga,berat,subharga)
                    VALUES ('$id_penjualan_barusan','$id_produk','$jumlah','$nama_produk','$harga','$berat','$subharga')");


      // update stok produk
      // $koneksi->query("UPDATE produk SET stok_produk=stok_produk -$jumlah WHERE
      //               id_produk = '$id_produk'");
    }

    unset($_SESSION["keranjang"]);
    if ($method_pembayaran=="cashless") {
    echo "<script>location='nota.php?id=$id_penjualan_barusan&pembayaran=cashless';</script>";
    }else{
      echo "<script>location='nota.php?id=$id_penjualan_barusan&pembayaran=cash';</script>";
    }
  }
  ?>
  <!-- end food section -->

  <!-- footer section -->
  <footer class="footer_section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 footer-col">
          <div class="footer_contact">
            <h4>
              Contact Us
            </h4>
            <div class="contact_link_box">
              <a href="about.php">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span>
                  Griya Alam Sentosa (GAS) Blok Y, Cileungsi
                </span>
              </a>
              <a href="https://wa.me/6281288939383">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span>
                  +6281288939383
                </span>
              </a>
              <a href="mailto:tamanbelakang@gmail.com">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span>
                  tamanbelakang@gmail.com
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-col">
          <div class="footer_detail">
            <a href="" class="footer-logo">
              TAMAN BELAKANG
            </a>

            <div class="footer_social">
              <a href="https://www.instagram.com/tamanbelakang.gas/" target="_blank">
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-col">
          <h4>
            Jam Buka
          </h4>
          <p>
            Setiap Hari
          </p>
          <p>
            18.00 - 23.00
          </p>
        </div>
      </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> All Rights Reserved By TAMAN BELAKANG<br><br>
        </p>
      </div>
    </div>
  </footer>
  <!-- footer section -->

  <!-- jQery -->
  <script src="js/jquery-3.4.1.min.js"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script src="js/bootstrap.js"></script>
  <!-- owl slider -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- isotope js -->
  <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
  <!-- nice select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
  <!-- custom js -->
  <script src="js/custom.js"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
  </script>
  <!-- End Google Map -->

</body>

</html>
