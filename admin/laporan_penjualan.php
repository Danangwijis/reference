<?php
$semuadata = array();
$tgl_mulai = "-";
$tgl_selesai = "-";
if (isset($_POST["kirim"])) {
    $tgl_mulai = $_POST["tglm"];
    $tgl_selesai = $_POST["tgls"];

    $ambil = $koneksi->query("SELECT * FROM penjualan WHERE tanggal_pembelian BETWEEN '$tgl_mulai' AND '$tgl_selesai' AND status_pembelian = 'selesai'");
    while ($pecah = $ambil->fetch_assoc()) {
        $semuadata[] = $pecah;
    }
}
?>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->


    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


        <!-- Nav Item - Alerts -->


        <!-- Nav Item - Messages -->

        <div class="topbar-divider d-none d-sm-block"></div>
        <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
        <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
        <?php $pecah = $ambil->fetch_assoc() ?>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $pecah['nama_lengkap']; ?></span>
                <img class="img-profile rounded-circle" src="../images/assets/<?php echo $pecah['foto_admin'] ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profil
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?halaman=logout" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Keluar
                </a>
            </div>
        </li>

    </ul>

</nav>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Laporan Penjualan <?php echo $tgl_mulai ?> Hingga <?php echo $tgl_selesai ?></h1>
    </div>

</div>
<div class="card shadow mb-4">
    <div class="card-header py-3">


        <form method="post">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <input type="date" class="form-control" name="tglm" value="<?php echo $tgl_mulai ?>">

                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                        <input type="date" class="form-control" name="tgls" value="<?php echo $tgl_selesai ?>">

                    </div>
                </div>


                <div class="col-md-2">

                    <br>
                    <br>

                    <button class="btn btn-primary" name="kirim">Lihat</button>

                </div>
            </div>


        </form>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered"  id="printarea" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Pelanggan</th>
                            <th>Tanggal</th>
                            <th>Jumlah</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $total = 0; ?>
                        <?php foreach ($semuadata as $key => $value) : ?>
                            <?php $total += $value['total_pembelian'] ?>

                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $value['nama']; ?></td>
                                <td><?php echo $value['tanggal_pembelian']; ?></td>
                                <td><?php echo number_format($value['total_pembelian']); ?></td>
                                <td><?php echo $value['status_pembelian']; ?></td>

                            </tr>
                        <?php endforeach ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Total</th>
                            <th>Rp. <?php echo number_format($total) ?></th>

                        </tr>
                    </tfoot>
                </table>
                <a href="#" id="cetak">
                  <button type="button"  class="btn btn-info">
                    Cetak Laporan
                  </button>
                </a>

            </div>
        </div>
    </div>
</div>
