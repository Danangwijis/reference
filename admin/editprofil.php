<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->


    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


        <!-- Nav Item - Alerts -->


        <!-- Nav Item - Messages -->

        <div class="topbar-divider d-none d-sm-block"></div>
        <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
        <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
        <?php $pecah = $ambil->fetch_assoc() ?>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $pecah['nama_lengkap']; ?></span>
                <img class="img-profile rounded-circle" src="../images/assets/<?php echo $pecah['foto_admin'] ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profil
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?halaman=logout" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Keluar
                </a>
            </div>
        </li>

    </ul>

</nav>
<!-- End of Topbar -->

                    <div class=" container">
                        <div class="main-body">
                            <h3> Edit Profil</h3>
                            <form method="post" enctype="multipart/form-data">
                                <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
                                <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
                                <?php $pecah = $ambil->fetch_assoc() ?>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="d-flex flex-column align-items-center text-center">
                                                    <img src="../images/assets/<?php echo $pecah['foto_admin'] ?>" alt="Admin" class="rounded-circle p-1 bg-primary" width="150">
                                                    <br>
                                                    <input type="file" name="foto" class="form-control">
                                                    <div class="mt-3">
                                                        <h4><?php echo $pecah['nama_lengkap']; ?></h4>

                                                        <p class="text-muted font-size-sm"><?php echo $pecah['alamat_admin']; ?></p>

                                                    </div>
                                                </div>
                                                <hr class="my-4">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-8">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Username</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input type="text" class="form-control" value="<?php echo $pecah['username']; ?>" name="username">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Password</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input type="password" class="form-control" value="" name="password">
                                                        <label for="" style="color:red">*Jika tidak diubah dikosongkan saja</label>
                                                    </div>

                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Nama Lengkap </h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input type="text" class="form-control" value="<?php echo $pecah['nama_lengkap']; ?>" name="nama_lengkap">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Alamat</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input type="text" class="form-control" value="<?php echo $pecah['alamat_admin']; ?>" name="alamat_admin">
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">telepon</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input type="text" class="form-control" value="<?php echo $pecah['telepon']; ?>" name="telepon">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3"></div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <button class="btn btn-primary px-4" value="Save Changes" name="save"> Simpan </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php
                      if (isset($_POST['save'])) {
                          if($_POST['password']!=""){
                            $password = md5($_POST['password']);
                            $namafoto = $_FILES['foto']['name'];
                            $lokasifoto = $_FILES['foto']['tmp_name'];
                            if (file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name']))  {
                              move_uploaded_file($lokasifoto, "../images/assets/$namafoto");
                              $koneksi->query("UPDATE admin SET username='$_POST[username]',
                                password='$password',nama_lengkap='$_POST[nama_lengkap]',
                                telepon='$_POST[telepon]',alamat_admin='$_POST[alamat_admin]',foto_admin='$namafoto'
                                WHERE id_admin='$_GET[id]'");
                              }else {
                                $koneksi->query("UPDATE admin SET username='$_POST[username]',
                                  password='$password',nama_lengkap='$_POST[nama_lengkap]',
                                  telepon='$_POST[telepon]',alamat_admin='$_POST[alamat_admin]'
                                  WHERE id_admin='$_GET[id]'");
                              }
                            }
                            else{
                              $namafoto = $_FILES['foto']['name'];
                              $lokasifoto = $_FILES['foto']['tmp_name'];
                                  if (file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name']))  {
                                    move_uploaded_file($lokasifoto, "../images/assets/$namafoto");
                                    $koneksi->query("UPDATE admin SET username='$_POST[username]',
                                    nama_lengkap='$_POST[nama_lengkap]',
                                      telepon='$_POST[telepon]',alamat_admin='$_POST[alamat_admin]',foto_admin='$namafoto'
                                      WHERE id_admin='$_GET[id]'");
                                  }else {
                                    $koneksi->query("UPDATE admin SET username='$_POST[username]',
                                      nama_lengkap='$_POST[nama_lengkap]',
                                      telepon='$_POST[telepon]',alamat_admin='$_POST[alamat_admin]'
                                      WHERE id_admin='$_GET[id]'");
                                  }
                              }


                        ?>

                          <div class ='alert alert-info'>Data Tersimpan</div>
                          <meta http-equiv='refresh' content ="1;url=index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                    <?php } ?>
