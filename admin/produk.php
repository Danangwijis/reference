<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->


    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


        <!-- Nav Item - Alerts -->


        <!-- Nav Item - Messages -->

        <div class="topbar-divider d-none d-sm-block"></div>
        <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
        <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
        <?php $pecah = $ambil->fetch_assoc() ?>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $pecah['nama_lengkap']; ?></span>
                <img class="img-profile rounded-circle" src="../images/assets/<?php echo $pecah['foto_admin'] ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profil
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?halaman=logout" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Keluar
                </a>
            </div>
        </li>

    </ul>

</nav>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Produk</h1>


    </div>

</div>
<div class="container ">


    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="thetable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>no </th>
                        <th>nama </th>
                        <th>Kategori</th>
                        <th>Harga</th>
                        <th>foto </th>
                        <th>aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $nomor = 1; ?>
                    <?php $ambil = $koneksi->query("SELECT * FROM produk LEFT JOIN kategori ON produk.id_kategori=kategori.id_kategori"); ?>
                    <?php while ($pecah = $ambil->fetch_assoc()) { ?>
                        <tr>
                            <td><?php echo $nomor; ?></td>
                            <td><?php echo $pecah['nama_produk']; ?></td>
                            <td><?php echo $pecah['nama_kategori']; ?></td>
                            <td><?php echo $pecah['harga']; ?></td>
                            <td>
                                <img src="../images/BOSQUE/<?php echo $pecah['foto_produk']; ?>" width=100">
                            </td>
                            <td>
                                <a href="index.php?halaman=hapusproduk&id=<?php echo $pecah['id_produk']; ?> " class="btn btn-danger btn">hapus</a>
                                <a href="index.php?halaman=ubahproduk&id=<?php echo $pecah['id_produk']; ?> " class="btn btn-warning btn">ubah</a>
                            </td>
                        </tr>
                        <?php $nomor++; ?>
                    <?php } ?>
                </tbody>
            </table>
            <a href="index.php?halaman=tambahproduk" class="btn btn-primary">Tambah Data</a>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#thetable').DataTable();
    });
</script>

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap.min.js"></script>
