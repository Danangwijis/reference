﻿<?php
session_start();
$koneksi = new mysqli("localhost", "root", "", "taman_belakang");

if (isset($_SESSION["admin"])) {
  echo "<script>location='index.php';</script>";
}
?>
<!doctype html>

<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="assets/css/login.css">
  <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
  <!-- Bootstrap CSS -->
  <link href=" https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <script src="https://kit.fontawesome.com/7f67eb912b.js" crossorigin="anonymous"></script>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Google Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
  <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
  <title>Bosque</title>
</head>

<body>

  <section class="sign-in">
    <div class="container">
      <div class="signin-content">
        <div class="signin-image">
          <figure><img src="../images/assets/taman_belakang.jpg" alt="sing up image"></figure>

        </div>

        <div class="signin-form">
          <h2 class="form-title">Sign In</h2>
          <form role="form" method="post">
            <?php
            $notif = isset($_GET['notif']) ? $_GET['notif'] : false;
            if ($notif == true) {
              echo "<div class='notif'><h5><b>Maaf password anda salah, silahkan coba lagi</b></h5></div>";
            }
            ?>
            <div class="form-group">
              <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
              <input type="text" name="user" id="email " placeholder=" Your Name" />
            </div>
            <div class="form-group">
              <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
              <input type="password" name="pass" id="password" placeholder="Password" />
            </div>
            <div class="form-group">
              <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
              <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
            </div>
            <div class="form-group form-button">
              <input type="submit" name="login" id="login" class="form-submit" value="Log in" />
            </div>
          </form>
          <?php
          if (isset($_POST['login'])) {
            $password = md5($_POST['pass']);
            $ambil = $koneksi->query("SELECT * FROM admin WHERE username='$_POST[user]' AND
            password = '$password'");
            $cocok = $ambil->num_rows;
            if ($cocok == 1) {
              $_SESSION['admin'] = $ambil->fetch_assoc();
              echo "<div class ='alert alert-info'>login sukses</div>";
              echo "<meta http-equiv='refresh' content='1;url=index.php'>";
            } else {
              echo "<div class='alert alert-dange'>login Gagal</div>";
              echo "<meta http-equiv='refresh' content='1;url=login.php'>";
            }
          }
          ?>
        </div>
      </div>
    </div>
  </section>

  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Third party plugin JS-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
  <!-- Core theme JS-->
  <script src="js/scripts.js"></script>
