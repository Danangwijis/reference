<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


        <!-- Nav Item - Alerts -->


        <!-- Nav Item - Messages -->

        <div class="topbar-divider d-none d-sm-block"></div>
        <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
        <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
        <?php $pecah = $ambil->fetch_assoc() ?>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $pecah['nama_lengkap']; ?></span>
              <img class="img-profile rounded-circle" src="../images/assets/<?php echo $pecah['foto_admin'] ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Settings
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                    Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>

    </ul>
</nav>

<?php
$ambil = $koneksi->query("SELECT * FROM penjualan WHERE penjualan.id_penjualan='$_GET[id]'");
$detail = $ambil->fetch_assoc();
?>
<!--<pre><?php print_r($detail); ?></pre>-->





<div class="container-fluid">
    <h2> Detail Pembelian </h2>
    <div class="row">
        <div class="col-md-4">
            <h3>Pembelian</h3>
            <p>
                tanggal :<?php echo $detail['tanggal_pembelian']; ?> <br>
                total :<?php echo number_format($detail['total_pembelian']); ?><br>
                Status :<?php echo $detail['status_pembelian']; ?>
            </p>
        </div>
        <div class="col-md-4">
            <h3>Pelanggan</h3>
            <strong> <?php echo $detail['nama']; ?></strong> <br>
            <p>
                <?php echo $detail['nohandphone']; ?> <br>
            
                <strong>Nomor Meja :</strong><br>
                <?php echo $detail['no_meja']; ?> <br>
            </p>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>harga</th>
                        <th>Jumlah</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $nomor = 1; ?>
                    <?php $ambil = $koneksi->query("SELECT * FROM penjualan_produk JOIN produk ON
                penjualan_produk.id_produk=produk.id_produk
                WHERE penjualan_produk.id_penjualan='$_GET[id]' "); ?>
                    <?php while ($pecah = $ambil->fetch_assoc()) { ?>
                        <tr>
                            <td><?php echo $nomor; ?></td>
                            <td><?php echo $pecah['nama_produk']; ?></td>
                            <td><?php echo number_format($pecah['harga']); ?></td>
                            <td><?php echo $pecah['jumlah']; ?></td>
                            <td><?php echo number_format($pecah['harga'] * $pecah['jumlah']); ?></td>
                        </tr>
                        <?php $nomor++; ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
