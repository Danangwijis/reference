<div class="container-fluid">
    <br>
    <br>
    <h2> tambah Produk </h2>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Nama </label>
            <input type="text" class="form-control" name="nama">
        </div>
        <div class="form-group">
            <label>Kategori </label>
            <select class="form-control" name="kategori" value="Pilih Ukuran">
              <option value="1">Aneka Nasi</option>
              <option value="2">Nasi Goreng</option>
              <option value="3">Nasi Ayam</option>
              <option value="4">Nastel</option>
              <option value="5">Magelangan</option>
              <option value="6">Aneka Mie</option>
              <option value="7">Snack</option>
              <option value="8">Keju Aroma</option>
              <option value="9">Aneka Bakso</option>
              <option value="10">Aneka Sate</option>
              <option value="11">Minuman Panas</option>
              <option value="12">Minuman Dingin</option>
            </select>
        </div>

        <div class="form-group">
            <label>Harga</label>
            <input type="number" class="form-control" name="harga">
        </div>

        <div class="form-group">
            <label>Deskripsi </label>
            <textarea class="form-control" name="deskripsi" rows="10"></textarea>
        </div>
        <div class="form-group">
            <label>Foto </label>
            <div class="tempat-masukin" style="margin-bottom: 10px;"><input type="file" class="form-control" name="foto">
            </div>

        </div>
        <button class="btn btn-primary" name="save">Simpan </button>
    </form>
</div>
<?php
if (isset($_POST['save'])) {
    $nama = $_FILES['foto']['name'];
    $lokasi = $_FILES['foto']['tmp_name'];
    $kategori = $_POST["kategori"];
    move_uploaded_file($lokasi, "../foto_produk/" . $nama);
    $koneksi->query("INSERT INTO produk
    (nama_produk,id_kategori,harga,foto_produk,deskripsi_produk)
    VALUES('$_POST[nama]','$_POST[kategori]','$_POST[harga]','$nama','$_POST[deskripsi]')");


    echo "<div class ='alert alert-info'>Data Tersimpan</div>";
    echo "<meta http-equiv='refresh' content ='1;url=index.php?halaman=tambahproduk'>";
}
?>
