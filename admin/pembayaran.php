<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->


    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


        <!-- Nav Item - Alerts -->


        <!-- Nav Item - Messages -->

        <div class="topbar-divider d-none d-sm-block"></div>
        <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
        <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
        <?php $pecah = $ambil->fetch_assoc() ?>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $pecah['nama_lengkap']; ?></span>
                <img class="img-profile rounded-circle" src="../images/assets/<?php echo $pecah['foto_admin'] ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Settings
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                    Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>

    </ul>

</nav>

<?php

// dapetin id pembelian
$id_penjualan = $_GET['id'];

// ngambil data pembayaran dari id pembelian
$ambil = $koneksi->query(" SELECT * FROM pembayaran WHERE id_penjualan='$id_penjualan'");
$detail = $ambil->fetch_assoc();
?>
<div class="container-fluid">
    <H2> Data Konfirmasi Pembayaran </h2>
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <td><?php echo $detail['nama']; ?></td>

                        </tr>
                        <tr>
                            <th>Bank</th>
                            <td><?php echo $detail['bank']; ?></td>

                        </tr>
                        <tr>
                            <th>Jumlah</th>
                            <td>Rp . <?php echo number_format($detail['jumlah']); ?></td>
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <td><?php echo $detail['tanggal']; ?></td>
                        </tr>
                </table>
            </div>
            <div class="col-md-6">
                <img src="../bukti_pembayaran/<?php echo $detail['bukti'] ?>" alt="" class="img-fluid img-thumbnail">

            </div>
        </div>

        <form method="post">
            <div class="form-group">
                <label> No Resi Pengiriman </label>
                <input type="text" class="form-control" name="resi">
            </div>
            <div class="form-group">
                <label> Status </label>
                <select class="form-control" name="status">
                    <option value="">Pilih status</option>
                    <option value="Terkirim">Terkirim</option>
                    <option value="tolak">tolak</option>
                </select>

            </div>
            <button class="btn btn-primary" name="proses">Proses</button>
        </form>
        <?php
        if (isset($_POST["proses"])) {
            $resi = $_POST["resi"];
            $status = $_POST["status"];
            $koneksi->query("UPDATE penjualan set resi_pengiriman='$resi',status_pembelian='$status'
            WHERE id_penjualan='$id_penjualan'");

            echo "<script>alert('Data Penjualan sudah di update');</script>";
            echo "<script>location=:'index.php?halaman=penjualan';</script>";
        }
        ?>

    </div>
</div>