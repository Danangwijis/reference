<body>
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>

        <!-- Topbar Search -->


        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->


            <!-- Nav Item - Alerts -->


            <!-- Nav Item - Messages -->

            <div class="topbar-divider d-none d-sm-block"></div>
            <?php $id_admin = $_SESSION["admin"]['id_admin']; ?>
            <?php $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'"); ?>
            <?php $pecah = $ambil->fetch_assoc() ?>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $pecah['nama_lengkap']; ?></span>
                    <img class="img-profile rounded-circle" src="../images/assets/<?php echo $pecah['foto_admin'] ?>">
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="index.php?halaman=profil_admin&id=<?php echo $pecah["id_admin"]; ?>">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profil
                    </a>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?halaman=logout" data-toggle="modal" data-target="#logoutModal">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Keluar
                    </a>
                </div>
            </li>

        </ul>

    </nav>

    <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">Data Penjualan</h1>
              <a href="index.php?halaman=laporan_penjualan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Laporan Penjualan</a>

          </div>

      </div>
    <div class="container">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="thetable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Customer</th>
                            <th>Tanggal</th>
                            <th>Status Pembelian </th>
                            <th>No Meja </th>
                            <th>Total</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form method="post">
                            <?php $nomor = 1; ?>
                            <?php $ambil = $koneksi->query("SELECT * FROM penjualan ORDER BY status_pembelian ASC, tanggal_pembelian DESC"); ?>
                            <?php while ($pecah = $ambil->fetch_assoc()) { ?>
                                <tr>
                                    <td><?php echo $nomor; ?></td>
                                    <td><?php echo $pecah['nama']; ?></td>
                                    <td><?php echo $pecah['tanggal_pembelian']; ?></td>
                                    <td><?php echo $pecah['status_pembelian']; ?>
                                    <td><?php echo $pecah['no_meja']; ?>


                                    <td><?php echo $pecah['total_pembelian']; ?></td>
                                    <td><a href="index.php?halaman=detail&id=<?php echo $pecah['id_penjualan']; ?> " class="btn btn-info">Detail</a>
                                        <?php if ($pecah['status_pembelian'] == "pending") : ?>
                                            <a href="index.php?halaman=selesaipenjualan&id=<?php echo $pecah['id_penjualan']; ?>" class="btn btn-success"> Selesai</button>
                                            <?php endif ?>

                                    </td>
                                </tr>
                                <?php $nomor++; ?>
                            <?php } ?>
                        </form>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#thetable').DataTable();
        });
    </script>


</body>
